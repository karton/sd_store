FactoryGirl.define do
  factory :book do
    publisher
    name { Faker::Book.title }
    available true

    transient do
      authors_count 2
      genres_count 2
    end

    after(:create) do |book, evaluator|
      create_list(:author, evaluator.authors_count, books: [book])
      create_list(:genre, evaluator.genres_count, books: [book])
    end
  end
end
