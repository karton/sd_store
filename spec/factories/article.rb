FactoryGirl.define do
  factory :article do
    name { Faker::Book.title }
    body { Faker::StarWars.quote }

    transient do
      authors_count 2
      genres_count 2
    end

    after(:create) do |article, evaluator|
      create_list(:author, evaluator.authors_count, articles: [article])
      create_list(:genre, evaluator.genres_count, articles: [article])
    end
  end
end
