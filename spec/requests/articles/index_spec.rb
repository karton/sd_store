require 'rails_helper'

describe 'GET /articles', type: :request do
  context 'when there are no articles' do
    subject { response }

    before { get articles_path }

    it { is_expected.to be_success }
    it { expect(subject.parsed_body).to eq 'ok' => true, 'data' => [] }
  end

  context 'when there are some articles' do
    let!(:articles) { create_list(:article, 2) }

    subject { response }

    before { get articles_path }

    it { is_expected.to be_success }
    it { expect(subject.parsed_body['data'].size).to eq 2 }

    describe 'attributes' do
      let(:expected_attributes) do
        {
          'id' => a_kind_of(Integer),
          'name' => a_kind_of(String).and(be_present),
          'body' => a_kind_of(String).and(be_present),
          'genres' => all(
            match(
              'id' => a_kind_of(Integer),
              'name' => a_kind_of(String).and(be_present)
            )
          ),
          'authors' => all(
            match(
              'id' => a_kind_of(Integer),
              'name' => a_kind_of(String).and(be_present)
            )
          )
        }
      end

      subject { response.parsed_body['data'].first }

      it 'returns appropriate attributes' do
        expect(subject).to match(expected_attributes)
      end
    end
  end
end
