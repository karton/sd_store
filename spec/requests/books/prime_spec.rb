require 'rails_helper'

describe 'GET /books/prime', type: :request do
  context 'when there are no books' do
    subject { response }

    before { get prime_books_path }

    it { is_expected.to be_success }
    it { expect(subject.parsed_body).to eq 'ok' => true, 'data' => [] }
  end

  context 'where there are no prime books' do
    let!(:books) do
      [
        create(:book, name: 'The Curious Incident of the Dog in the Night-Time'),
        create(:book, name: 'Far From the Madding Crowd')
      ]
    end

    subject { response }

    before { get prime_books_path }

    it { is_expected.to be_success }
    it { expect(subject.parsed_body['data']).to eq [] }
  end

  context 'when there are both prime and non-prime books' do
    let!(:prime_books) do
      [
        create(:book, name: 'Specimen Days'),
        create(:book, name: 'All Passion Spent')
      ]
    end

    let!(:non_prime_books) do
      [
        create(:book, name: 'The Curious Incident of the Dog in the Night-Time'),
        create(:book, name: 'Far From the Madding Crowd')
      ]
    end

    subject { response }

    before { get prime_books_path }

    it { is_expected.to be_success }
    it { expect(subject.parsed_body['data'].size).to eq 2 }
  end
end
