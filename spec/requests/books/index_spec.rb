require 'rails_helper'

describe 'GET /books', type: :request do
  context 'when there are no books' do
    subject { response }

    before { get books_path }

    it { is_expected.to be_success }
    it { expect(subject.parsed_body).to eq 'ok' => true, 'data' => [] }
  end

  context 'when there are some books' do
    let!(:books) { create_list(:book, 2) }

    subject { response }

    before { get books_path }

    it { is_expected.to be_success }
    it { expect(subject.parsed_body['data'].size).to eq 2 }

    describe 'attributes' do
      let(:expected_attributes) do
        {
          'id' => a_kind_of(Integer),
          'name' => a_kind_of(String).and(be_present),
          'available' => true,
          'publisher' => {
            'id' => a_kind_of(Integer),
            'name' => a_kind_of(String).and(be_present)
          },
          'genres' => all(
            match(
              'id' => a_kind_of(Integer),
              'name' => a_kind_of(String).and(be_present)
            )
          ),
          'authors' => all(
            match(
              'id' => a_kind_of(Integer),
              'name' => a_kind_of(String).and(be_present)
            )
          )
        }
      end

      subject { response.parsed_body['data'].first }

      it 'returns appropriate attributes' do
        expect(subject).to match(expected_attributes)
      end
    end
  end
end
