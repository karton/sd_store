# Store

### Side effects and possible scalability issues:
  * Far too many records/fields are returned in `GET /books` and `GET /articles`
    * Switch to [JSON API](http://jsonapi.org/examples/)
      * Specify whether to include associations `GET /articles?include=authors HTTP/1.1`
      * Prevent loading the entire list of fields `GET /articles?include=author&fields[articles]=title&fields[author]=name HTTP/1.1`
      * Use pagination `GET /articles?page[number]=3&page[size]=1 HTTP/1.1`
  * Too many joins are in use when querying for books and articles.
    Probably, this can be an issue in the near future
  * Paginating `GET /search/filter` can become a bit tricky because two models (books and articles) are in use
  * Two queries are involved in `GET /search/filter`. They execute sequentially, therefore when one of them slows down the entire search slows down 
  * If you're looking for books from a certain publisher, you won't get any articles `GET /search/filter?name_eq=&authors_name_eq=Jacinthe Fay DDS&publisher_name_eq=Darakwon Press`
  * How `GET /books/prime` works
    * I've limited book name to 255 characters
    * Created [partial index](https://www.postgresql.org/docs/9.6/static/indexes-partial.html) with `WHERE lenth(name) IN(An array of primes)`
    * Created `Books::Queries::PrimeBooks` with the same query conditions
    * Verified that the Postgres query planner uses the index `Bitmap Index Scan on index_books_on_name`
    * On my laptop, the query performs under 22 ms on average