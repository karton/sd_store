# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170407205829) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_trgm"

  create_table "articles", force: :cascade do |t|
    t.string   "name"
    t.text     "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "articles_authors", id: false, force: :cascade do |t|
    t.integer "author_id",  null: false
    t.integer "article_id", null: false
    t.index ["article_id"], name: "index_articles_authors_on_article_id", using: :btree
    t.index ["author_id"], name: "index_articles_authors_on_author_id", using: :btree
  end

  create_table "articles_genres", id: false, force: :cascade do |t|
    t.integer "article_id", null: false
    t.integer "genre_id",   null: false
    t.index ["article_id"], name: "index_articles_genres_on_article_id", using: :btree
    t.index ["genre_id"], name: "index_articles_genres_on_genre_id", using: :btree
  end

  create_table "authors", force: :cascade do |t|
    t.string "name", limit: 255, null: false
  end

  create_table "authors_books", id: false, force: :cascade do |t|
    t.integer "author_id", null: false
    t.integer "book_id",   null: false
    t.index ["author_id"], name: "index_authors_books_on_author_id", using: :btree
    t.index ["book_id"], name: "index_authors_books_on_book_id", using: :btree
  end

  create_table "books", force: :cascade do |t|
    t.string   "name",         limit: 255, null: false
    t.boolean  "available"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.integer  "publisher_id"
    t.index ["name"], name: "index_books_on_name", where: "(length((name)::text) = ANY (ARRAY[2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251]))", using: :btree
    t.index ["publisher_id"], name: "index_books_on_publisher_id", using: :btree
  end

  create_table "books_genres", id: false, force: :cascade do |t|
    t.integer "book_id",  null: false
    t.integer "genre_id", null: false
    t.index ["book_id"], name: "index_books_genres_on_book_id", using: :btree
    t.index ["genre_id"], name: "index_books_genres_on_genre_id", using: :btree
  end

  create_table "genres", force: :cascade do |t|
    t.string "name", limit: 255, null: false
  end

  create_table "lists", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "publishers", force: :cascade do |t|
    t.string "name", limit: 255, null: false
  end

  create_table "single_sorts", force: :cascade do |t|
    t.integer  "list_id"
    t.integer  "book_id"
    t.integer  "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
