class RemoveBooksAuthorsColumn < ActiveRecord::Migration[5.0]
  def up
    remove_column :books, :authors
  end
end
