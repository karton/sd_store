class DropArticlesAuthorsColumn < ActiveRecord::Migration[5.0]
  def up
    remove_column :articles, :authors
  end
end
