class CreateArticlesGenres < ActiveRecord::Migration[5.0]
  def change
    create_join_table :articles, :genres do |t|
      t.index :article_id
      t.index :genre_id
    end
  end
end
