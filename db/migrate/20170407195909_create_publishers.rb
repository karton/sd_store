class CreatePublishers < ActiveRecord::Migration[5.0]
  def change
    create_table :publishers do |t|
      t.string :name, limit: 255, null: false
    end

    add_column :books, :publisher_id, :integer
    add_index :books, :publisher_id
  end
end
