class RemoveGenresFromArticles < ActiveRecord::Migration[5.0]
  def up
    remove_column :articles, :genres
  end
end
