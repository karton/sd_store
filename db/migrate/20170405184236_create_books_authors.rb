class CreateBooksAuthors < ActiveRecord::Migration[5.0]
  def change
    create_join_table :authors, :books do |t|
      t.index :book_id
      t.index :author_id
    end
  end
end
