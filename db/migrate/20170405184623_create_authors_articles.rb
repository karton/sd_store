class CreateAuthorsArticles < ActiveRecord::Migration[5.0]
  def change
    create_join_table :authors, :articles do |t|
      t.index :article_id
      t.index :author_id
    end
  end
end
