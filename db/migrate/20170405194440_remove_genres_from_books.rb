class RemoveGenresFromBooks < ActiveRecord::Migration[5.0]
  def up
    remove_column :books, :genres
  end
end
