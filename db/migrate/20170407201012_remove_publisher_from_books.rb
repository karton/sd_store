class RemovePublisherFromBooks < ActiveRecord::Migration[5.0]
  def up
    remove_column :books, :publisher
  end
end
