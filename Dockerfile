FROM ruby:2.3.3

RUN apt-get update -qq && apt-get install -y --no-install-recommends \
    git \
    build-essential \
    libpq-dev \
    postgresql-client \
    nodejs \
  && rm -rf /var/lib/apt/lists/*

ENV APP_HOME /app
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

ADD Gemfile* $APP_HOME/
RUN bundle install --jobs=4 --retry=3

ADD . $APP_HOME/
