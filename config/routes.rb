Rails.application.routes.draw do
  resources :articles, only: :index
  resources :books, only: :index do
    collection do
      get :prime
    end
  end
  get 'search/filter', to: 'search#index'
end
