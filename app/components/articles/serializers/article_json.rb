module Articles
  module Serializers
    class ArticleJSON < Common::Serializers::BaseJSON
      def as_json(_options = {})
        {
          id: object.id,
          name: object.name,
          body: object.body,
          genres: object.genres,
          authors: object.authors
        }
      end
    end
  end
end
