module Articles
  module Queries
    class SearchArticles < Common::Queries::Base
      include Common::Queries::Searchable

      ATTRIBUTES = %w[name_eq authors_name_eq genres_name_eq].freeze

      def searchable_attributes
        ATTRIBUTES
      end

      private

      def default_scope
        AllArticles.new.call
      end
    end
  end
end
