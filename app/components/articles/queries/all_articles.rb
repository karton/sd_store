module Articles
  module Queries
    class AllArticles < Common::Queries::Base
      def call
        scope.eager_load(:genres, :authors)
      end

      private

      def default_scope
        Article.all
      end
    end
  end
end
