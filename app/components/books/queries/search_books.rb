module Books
  module Queries
    class SearchBooks < Common::Queries::Base
      include Common::Queries::Searchable

      ATTRIBUTES = %w[name_eq authors_name_eq genres_name_eq publisher_name_eq].freeze

      def searchable_attributes
        ATTRIBUTES
      end

      private

      def default_scope
        AllBooks.new.call
      end
    end
  end
end
