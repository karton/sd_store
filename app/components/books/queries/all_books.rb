module Books
  module Queries
    class AllBooks < Common::Queries::Base
      def call
        scope.eager_load(:genres, :authors, :publisher)
      end

      private

      def default_scope
        Book.all
      end
    end
  end
end
