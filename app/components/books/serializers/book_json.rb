module Books
  module Serializers
    class BookJSON < Common::Serializers::BaseJSON
      def as_json(_options = {})
        {
          id: object.id,
          name: object.name,
          available: object.available,
          publisher: object.publisher,
          genres: object.genres,
          authors: object.authors
        }
      end
    end
  end
end
