module Common
  module Serializers
    class BaseJSON
      def initialize(object)
        @object = object
      end

      def as_json(_options = {})
        raise NotImplementedError
      end

      private

      attr_reader :object
    end
  end
end
