module Common
  module Queries
    module Searchable
      def call(attributes)
        scope.ransack(attributes).result
      end

      def searchable_attributes
        raise NotImplementedError
      end

      def searchable?(attributes)
        attributes.all? do |name, value|
          searchable_attributes.include?(name) || value.blank?
        end
      end
    end
  end
end
