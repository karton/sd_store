module Common
  module Queries
    class Base
      def initialize(scope = default_scope)
        @scope = scope
      end

      private

      attr_reader :scope

      def default_scope
        raise NotImplementedError
      end
    end
  end
end
