class BooksController < ApplicationController
  def index
    books = Books::Queries::AllBooks.new.call.to_a
    json_response(data: serialize_collection(books))
  end

  def prime
    books = Books::Queries::PrimeBooks.new.call.to_a
    json_response(data: serialize_collection(books))
  end

  private

  def serialize_collection(books)
    books.map { |book| serialize(book) }
  end

  def serialize(book)
    Books::Serializers::BookJSON.new(book)
  end
end
