class ApplicationController < ActionController::API
  def json_response(status: :ok, data:)
    render json: { ok: status == :ok, data: data }, status: status
  end
end
