class ArticlesController < ApplicationController
  def index
    json_response(data: articles)
  end

  private

  def articles
    all_articles = Articles::Queries::AllArticles.new.call
    all_articles.map { |article| serialize(article) }
  end

  def serialize(article)
    Articles::Serializers::ArticleJSON.new(article)
  end
end
