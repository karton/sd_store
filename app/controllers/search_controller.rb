class SearchController < ApplicationController
  def index
    json_response(
      data: {
        articles: articles,
        books: books
      }
    )
  end

  private

  def articles
    return [] unless articles_query.searchable?(permitted_params)

    all_articles = articles_query.call(permitted_params)
    all_articles.map { |article| serialize_article(article) }
  end

  def books
    return [] unless books_query.searchable?(permitted_params)

    all_books = books_query.call(permitted_params)
    all_books.map { |book| serialize_book(book) }
  end

  def articles_query
    @_articles_query ||= Articles::Queries::SearchArticles.new
  end

  def books_query
    @_books_query ||= Books::Queries::SearchBooks.new
  end

  def permitted_params
    queries = [articles_query, books_query]
    searchable_attributes = queries.sum(&:searchable_attributes).uniq
    params.permit(*searchable_attributes).to_h
  end

  def serialize_article(article)
    Articles::Serializers::ArticleJSON.new(article)
  end

  def serialize_book(book)
    Books::Serializers::BookJSON.new(book)
  end
end
